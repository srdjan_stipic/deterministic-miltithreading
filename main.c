/* 
 * An example with a different number of transactions in each thread:
 * ThreadN: executes N transactions, N={0,1,2}.
 */
#include <stdio.h>
#define __USE_GNU
#include <pthread.h>
#undef __USE_GNU

pthread_barrier_t barr;

int ggggg;

__attribute__((transaction_pure))
void print_tid() {
  fprintf(stderr, "thread %ld\n", pthread_self());
  fprintf(stderr, "ggggg %d\n", ggggg);
}

void* g(void* arg) {
  unsigned int i, j;
  unsigned int* ii = (unsigned int*)arg;
  pthread_t tid = pthread_self();
  fprintf(stderr, "thread arg %d\n", *ii);
  fprintf(stderr, "thread %ld\n", tid);
  pthread_yield();
  fprintf(stderr, "thread %ld\n", tid);
  pthread_barrier_wait(&barr);

  for (i = 0; i < *ii; i++) {
    __transaction_atomic {
      for (j = 0; j < 5; j++) {
        ggggg++;
//        print_tid();
      }
    }
  }

  pthread_barrier_wait(&barr);

  return NULL;
}

volatile int xxxx = 1;
static void my_f() {
  while (xxxx) {};
}


int main() {
  pthread_t tid = pthread_self();
  unsigned int i;
  unsigned int N = 3;
  pthread_barrier_init(&barr, NULL, N+1);

// my_f();

  unsigned int ii[N];

  pthread_t t[N];
  for (i = 0; i < N; i++) {
    ii[i] = i;
    fprintf(stderr, "thread %ld\n", tid);
    pthread_create(&t[i], NULL, g, &ii[i]);
    pthread_yield();
  }


  fprintf(stderr, "start\n");
  pthread_barrier_wait(&barr);
  fprintf(stderr, "zzz\n");
  pthread_barrier_wait(&barr);
  fprintf(stderr, "xxx\n");

  for (i = 0; i < N; i++) {
    pthread_join(t[i], NULL);
  }

  pthread_barrier_destroy(&barr);

  fprintf(stderr, "ggggg %d\n", ggggg);

  fprintf(stderr, "End\n");

  return 0;
}
