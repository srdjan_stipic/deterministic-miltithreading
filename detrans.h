#ifndef DETRANS
#define DETRANS 1

#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <list>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <dlfcn.h>
#include <time.h>
#include <libitm.h>

#define GENERATE_WRAPPER(R_TYPE, NAME, ARGS_TYPE)              \
R_TYPE (*NAME##_orig) ARGS_TYPE = NULL;                        \
static inline void NAME##_init() {                             \
  if (NAME##_orig == NULL) {                                   \
    NAME##_orig = (R_TYPE(*)ARGS_TYPE)dlsym(RTLD_NEXT, #NAME); \
  }                                                            \
}

#ifdef TICKET_LOCK

#define lock_t struct arch_spinlock_t*

struct arch_spinlock_t;

extern "C" {
void ticket_spin_lock(arch_spinlock_t*);
void ticket_spin_unlock(arch_spinlock_t*);
void ticket_spin_init(arch_spinlock_t**);
void ticket_spin_destroy(arch_spinlock_t*);
}

#define LOCK(x) ticket_spin_lock(x)
#define UNLOCK(x) ticket_spin_unlock(x)
#define LOCK_INIT(x) ticket_spin_init(&x)
#define LOCK_DESTROY(x) ticket_spin_destroy(x)
#define YIELD() /* nothing */

#elif defined(SPIN_LOCK)

#define lock_t pthread_spinlock_t
#define LOCK(x) pthread_spin_lock(&x)
#define UNLOCK(x) pthread_spin_unlock(&x)
#define LOCK_INIT(x) pthread_spin_init(&x, 0)
#define LOCK_DESTROY(x) /* nothing */
#define YIELD() pthread_yield_orig()

#elif defined(MUTEX)

#define lock_t pthread_mutex_t
#define LOCK(x) pthread_mutex_lock(&x)
#define UNLOCK(x) pthread_mutex_unlock(&x)
#define LOCK_INIT(x) pthread_mutex_init(&x, 0)
#define LOCK_DESTROY(x) /* nothing */
#define YIELD() /* nothing */

#endif

lock_t m;

bool preloaded = false;

using namespace std;

pthread_t first_tid;

#ifdef ADHOC_SUPPORT
#ifndef SAMPLE_PERIOD
#warning "SAMPLE_PERIOD not defined"
#define SAMPLE_PERIOD 10000
#else
#warning "SAMPLE_PERIOD defined"
#endif
#endif

/*extern "C"
void _ITM_initializeThread();
extern "C"
void _ITM_finalizeThread();
*/

extern "C" {
void pre_stm_start();
void post_stm_start();
void pre_stm_commit();
void post_stm_commit();
}

static unique_ptr<list<pthread_t> > ready_list;
static list<pthread_t> zombies;
static unordered_map<pthread_t, vector<pthread_t> > blocked;
static unique_ptr<list<pthread_t> > blocked_txns;
static unordered_map<pthread_barrier_t*, vector<pthread_t> > barriers;
static unordered_map<pthread_barrier_t*, unsigned> barriers_size;

static __thread pthread_t tid;

#ifdef DEBUG
__attribute__((transaction_pure))
int fprintf(FILE *stream, const char *format, ...);
#define DEBUG_PRINT(...) do{ fprintf( stderr, __VA_ARGS__ ); } while( false )
#define DEBUG_PRINT2(...) do{ } while ( false )
#define DEBUG_PRINT_STATE() /* nothing */

#elif defined DEBUG2
__attribute__((transaction_pure))
int fprintf(FILE *stream, const char *format, ...);
#define DEBUG_PRINT(...) do{ fprintf( stderr, __VA_ARGS__ ); } while( false )
#define DEBUG_PRINT2(...) do{ fprintf( stderr, __VA_ARGS__ ); } while( false )
#define DEBUG_PRINT_STATE() print_state()

static list<pthread_t> map_to_list(unordered_map<pthread_t, vector<pthread_t> > b);
static void print_list_unlocked(list<pthread_t> l, const char*s);
static void print_state();

#else /* !DEBUG */
#define DEBUG_PRINT(...) do{ } while ( false )
#define DEBUG_PRINT2(...) do{ } while ( false )
#define DEBUG_PRINT_STATE() /* nothing */
#endif

typedef struct my_pair {
  void*arg;
  void*(*f)(void*);
} my_pair;


void erase(list<pthread_t> &vec, pthread_t elem);
int contains(list<pthread_t> l, pthread_t elem);

#ifndef SERIAL_RUN
__attribute__((transaction_pure))
void push_back_from_front();
#endif

#ifdef ADHOC_SUPPORT
static void perf_event_init();
static void perf_event_close();
static void perf_counter_enable();
static void perf_counter_disable();
static void perf_counter_restart();
#endif // ADHOC_SUPPORT

#endif  // DETRANS
