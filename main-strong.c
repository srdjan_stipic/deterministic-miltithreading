/* 
 * An example with weak atomocity. One thread increments ggggg 50 times within a transaction,
 * another thread increments ggggg 50 times outside of transaction.
 * Non-deterministic execution: the final ggggg value can be any between 50 and 100.
 * Deterministic exexcution: the final ggggg value is always 100.
 */


#include <stdio.h>
#include <pthread.h>
#include <libitm.h>

pthread_barrier_t barr;

int ggggg;


extern void pre_stm_start();
extern void post_stm_start();
extern void pre_stm_commit();
extern void post_stm_commit();

__attribute__((transaction_pure))
void print_tid() {
  fprintf(stderr, "thread %lu\n", (unsigned long)pthread_self());
  fprintf(stderr, "ggggg %d\n", ggggg);
}

void* my_function(void* arg) {
  unsigned int j;
  unsigned int* ii = (unsigned int*)arg;
  pthread_t tid = pthread_self();
  fprintf(stderr, "thread arg %d\n", *ii);
  fprintf(stderr, "main: thread %lu\n", (unsigned long)tid);
  if (*ii != 0) {
   // pthread_yield();
    pthread_barrier_wait(&barr);

      __transaction_atomic {
        for (j = 0; j < 50; j++) {
          ggggg++;
        }
    }
    pthread_barrier_wait(&barr);
  }
  else {
   // pthread_yield();
   // pthread_yield();
   // pthread_yield();
   // pthread_yield();
    for (j = 0; j < 50; j++) {
      fprintf(stderr, "No txn: ggggg = %d\n", ggggg);
      ggggg++;
    }
  }
  return NULL;
}

/*volatile int xxxx = 1;
static void my_f() {
  while (xxxx) {};
}*/


int main() {
  pthread_t tid = pthread_self();
  unsigned int i;
  unsigned int N = 2;
  pthread_barrier_init(&barr, NULL, N);

// my_f();

  unsigned int ii[N];

pre_stm_start();
post_stm_start();
pre_stm_commit();
post_stm_commit();

  pthread_t t[N];
  for (i = 0; i < N; i++) {
    ii[i] = i;
    fprintf(stderr, "main: thread %lu\n",(unsigned long)tid);
    pthread_create(&t[i], NULL, my_function, &ii[i]);
   // pthread_yield();
  }

  fprintf(stderr, "start\n");
  pthread_barrier_wait(&barr);
  fprintf(stderr, "zzz\n");
  pthread_barrier_wait(&barr);
  fprintf(stderr, "xxx\n");

  for (i = 0; i < N; i++) {
    pthread_join(t[i], NULL);
  }

  pthread_barrier_destroy(&barr);

  fprintf(stderr, "ggggg %d\n", ggggg);

  fprintf(stderr, "End\n");

  return 0;
}
