#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_barrier_t b;

#ifdef TICKET_LOCK

#define lock_t struct arch_spinlock_t*
#define LOCK(x) ticket_spin_lock(x)
#define UNLOCK(x) ticket_spin_unlock(x)
#define LOCK_INIT(x) ticket_spin_init(&x)
#define LOCK_DESTROY(x) ticket_spin_destroy(x)

#elif defined(SPIN_LOCK)

#define lock_t pthread_spinlock_t
#define LOCK(x) pthread_spin_lock(&x)
#define UNLOCK(x) pthread_spin_unlock(&x)
#define LOCK_INIT(x) pthread_spin_init(&x, 0)
#define LOCK_DESTROY(x) /* nothing */

#elif defined(MUTEX)

#define lock_t pthread_mutex_t
#define LOCK(x) pthread_mutex_lock(&x)
#define UNLOCK(x) pthread_mutex_unlock(&x)
#define LOCK_INIT(x) pthread_mutex_init(&x, 0)
#define LOCK_DESTROY(x) /* nothing */

#endif

lock_t lock;

long l;
volatile int running;

#define N 2

void *f(void*arg) {
  int n = (int)(long)arg;
  pthread_barrier_wait(&b);
  while (running) {
    LOCK(lock);
    if ((l%N) == n) {
      ++l;
    }
    UNLOCK(lock);
    // pthread_yield();
  }
  return NULL;
}

int main() {
  LOCK_INIT(lock);
  long i;
  running = 1;
  pthread_barrier_init(&b, NULL, N+1);
  pthread_t t[N];
  for (i = 0; i < N; i++) {
    pthread_create(&t[i], NULL, f, (void*)i);
  }
  pthread_barrier_wait(&b);
  sleep(1);
  running = 0;
  for (i = 0; i < N; i++) {
    pthread_join(t[i],NULL);
  }
  LOCK_DESTROY(lock);
  printf("l = %ld\n", l);
  return 0;
}
