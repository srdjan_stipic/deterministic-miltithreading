CFLAGS += -O3 -Wall -Wextra -Wconversion -fgnu-tm
# -g -O0 -DDEBUG2

all: libt.so.1.0.1 libt-adhoc.so.1.0.1 adhoc

ticket_lock.o: ticket_lock.c
	gcc $(CFLAGS) ticket_lock.c -c -fPIC -ldl -lpthread

libt.so.1.0.1: ticket_lock.o detrans-wrappers.c detrans.h
	g++ $(CFLAGS) detrans-wrappers.c -c --std=c++11 -fPIC -DTICKET_LOCK -ldl -lpthread -I../tinystm/abi/gcc
	g++ -shared -Wl,-soname,libt.so.1 -o libt.so.1.0.1 detrans-wrappers.o ticket_lock.o -ldl -lstdc++

libt-adhoc.so.1.0.1: ticket_lock.o detrans-wrappers.c detrans.h
	g++ $(CFLAGS) detrans-wrappers.c -c --std=c++11 -fPIC -DTICKET_LOCK -DADHOC_SUPPORT -DSAMPLE_PERIOD=$(SAMPLE_PERIOD) -ldl -lpthread -I../tinystm/abi/gcc
	g++ -shared -Wl,-soname,libt.so.1 -o libt-adhoc.so.1.0.1 detrans-wrappers.o ticket_lock.o -ldl -lstdc++

final:
	g++ $(CFLAGS) main-strong.c -ldl -litm -lpthread -L../tinystm/abi/gcc -I../tinystm/abi/gcc

adhoc: main-adhoc.c
	g++ $(CFLAGS) main-adhoc.c -o adhoc -ldl -litm -lpthread -L../tinystm/abi/gcc -I../tinystm/abi/gcc

clean:
	rm *.o libt*
