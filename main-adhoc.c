#include <stdio.h>
#include <pthread.h>
#include <libitm.h>

extern void pre_stm_start();
extern void post_stm_start();
extern void pre_stm_commit();
extern void post_stm_commit();

volatile int start;
long workerLoopCount;

void* my_function(void*) {
  double c = 0;
  long i = 0;

  start = 1;
 
  c = 0.0012;
  for(i = 0; i < workerLoopCount; i++) {
    c += 0.01;
  }

  fprintf(stderr, "End worker thread, c = %f\n", c);

  return NULL;
}

int main(int argc, char** argv) {
  long loopCount = 0;
  double c = 0;
  long i = 0;

  if (argc == 2) {
    loopCount = atoi(argv[1]);
  }
  else if (argc == 3) {
    loopCount = atoi(argv[1]);
    workerLoopCount = atoi(argv[2]);
  }

  pre_stm_start();
  post_stm_start();
  pre_stm_commit();
  post_stm_commit();

  pthread_t t;

  pthread_create(&t, NULL, my_function, NULL);

  fprintf(stderr, "Before loop.\n"); 

  while (!start) {}

  c = 0.0012;
  for(i = 0; i < loopCount; i++) {
    c += 0.01;
  }
 
  pthread_join(t, NULL);

  fprintf(stderr, "End, c = %f\n", c);

  return 0;
}
