#include <immintrin.h>

typedef unsigned int u32;

#define __HLE_ACQUIRE ".byte 0xf2 ; "
#define __HLE_RELEASE ".byte 0xf3 ; "
#define READ_ONCE(x) (*(volatile typeof(&x))(&x))
#define mem_barr() asm volatile ("":::"memory")

static inline void __hle_xchg (volatile u32* addr)
{
  u32 value = 1;

  asm volatile (__HLE_ACQUIRE "lock; xchgl %1,%0"
  : "+r" (value), "+m" (*addr)
  :: "memory");
}

static inline void __hle_move (volatile u32* addr)
{
  asm volatile (__HLE_RELEASE "movl $0,%0"
  : "+m" (*addr) :: "memory");
}

static inline void tx_begin (volatile u32* addr) 
{
  __hle_xchg (addr);
}

static inline void tx_commit (volatile u32* addr)
{
  __hle_move (addr);
}
//////////////////////////////////////////////////////////////////////

#define SIZE (4096)
int a[SIZE];

__thread int b;

void f() {
  int i;
  for (i = 0; i < SIZE; i++) {
    a[i]++;
    b++;
    mem_barr();
  }
}

static void do_hle (int count)
{
  u32 data = 0;
  int p1;

  for (p1 = 0 ; p1 < count ; ++ p1)
  {
    tx_begin(&data); //__hle_xchg (& data);
    f();
    tx_commit(&data); //__hle_move (& data);
  }
}

//////////////////////////////////////////////////////////////////////

int main (int argc, char* argv [])
{
  int count = atoi (argv [1]);
  do_hle (count);
  return 0;
}
