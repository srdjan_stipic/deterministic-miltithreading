/* ioctl_period.c					*/
/* Test the PERF_EVENT_IOC_PERIOD ioctl			*/

/* by Vince Weaver   vincent.weaver _at_ maine.edu	*/

/* Linux 3.14 updated all architectures to match 	*/
/*	the ARM behavior.				*/

/* Since 3.7 (3581fe0ef37) ARM behaves differently	*/
/*	and updates the period immediately rather	*/
/*	than after the next overflow.			*/

/* This ioctl was broken until 2.6.36			*/
/*	ad0cf3478de8677f720ee06393b3147819568d6a	*/
/*	and always returned EFAULT			*/


#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <linux/perf_event.h> 
#include <linux/hw_breakpoint.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <asm/unistd.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <fcntl.h>

#include <errno.h>

#include <pthread.h>

#define OVERFLOWS_TO_TRACK 256	
static __thread long long overflow_counts[OVERFLOWS_TO_TRACK];
static __thread long long br_counts[OVERFLOWS_TO_TRACK];
static __thread int overflows=0;
int ov=0;

static long long sample_period;
static long long sample_period_new;
static __thread int fd_ins;
static __thread int fd_br;
static volatile int start;
static int workerLoopCount;
static int adhoc;
static int multithreaded;
int fdi_m;
int fdb_m;
int fdi_t;
int fdb_t;


long perf_event_open(struct perf_event_attr* event_attr, pid_t pid, int cpu, int group_fd, unsigned long flags)
{
    return syscall(__NR_perf_event_open, event_attr, pid, cpu, group_fd, flags);
}

pid_t gettid(void) {
    return syscall(__NR_gettid);
}

static void our_handler(int signum, siginfo_t *info, void *uc) {

	int fd = info->si_fd;
	int ret;
	long long value, value2;
	int read_result;

	if (fd == fd_ins) {
		/* Turn off measurement */
		ioctl(fd_ins,PERF_EVENT_IOC_DISABLE,0);

		/* Read out value */
		read_result=read(fd,&value,sizeof(long long));
		read_result=read(fd_br,&value2,sizeof(long long));
	
		overflow_counts[overflows]=value;
		br_counts[overflows]=value2;

		/* Increment, but make sure we don't overflow */
		overflows++;
		if (overflows>=OVERFLOWS_TO_TRACK) {
			overflows=OVERFLOWS_TO_TRACK-1;
		}

		if (overflows==5) {
			ret=ioctl(fd, PERF_EVENT_IOC_PERIOD,&sample_period_new);
			if (ret<0) {
			}
		}
		/* Restart for one more sample period */
		ret=ioctl(fd, PERF_EVENT_IOC_RESET,0);
		ret=ioctl(fd_br, PERF_EVENT_IOC_RESET,0);
		ret=ioctl(fd, PERF_EVENT_IOC_REFRESH,1);
	}
	else {
		fprintf(stderr, "You shouldn't be here!\n");
	}
	(void)read_result;
}

void* f() {
	int i;
	if (multithreaded) {
		struct perf_event_attr pe, pe2;
		struct sigaction sa;

		memset(&pe,0,sizeof(struct perf_event_attr));

		pe.type=PERF_TYPE_RAW;
		pe.config=(0x00C0ULL) | (0x0000ULL); //instructions retired
		pe.disabled=1;
		pe.exclude_kernel=1;
		pe.exclude_hv=1;
		pe.sample_period=sample_period;
		pe.wakeup_events=1;
		fd_ins=perf_event_open(&pe,0,-1,-1,0);
	
		/* Set up overflow */
		memset(&sa, 0, sizeof(struct sigaction));
		sa.sa_sigaction = our_handler;
		sa.sa_flags = SA_SIGINFO;
		if (sigaction( SIGRTMIN+2, &sa, NULL) < 0) {
			printf("Error setting up signal handler\n");
		}
	
		memset(&pe2,0,sizeof(struct perf_event_attr));

		pe2.type=PERF_TYPE_RAW;
		pe2.config=(0x00C4ULL) | (0x0100ULL);  // branches retired
		pe2.disabled=0;
		pe2.exclude_kernel=1;
		pe2.exclude_hv=1;
		pe2.sample_period=sample_period;
		pe2.wakeup_events=1;
		fd_br=perf_event_open(&pe2,0,-1,fd_ins,0);

		fdi_t = fd_ins;
		fdb_t = fd_br;

		fcntl(fd_ins, F_SETFL, O_RDWR|O_NONBLOCK|O_ASYNC);
		fcntl(fd_ins, F_SETSIG, SIGRTMIN+2);
		fcntl(fd_ins, F_SETOWN,gettid());

		ioctl(fd_ins, PERF_EVENT_IOC_RESET,0);
		ioctl(fd_br, PERF_EVENT_IOC_RESET,0);
		ioctl(fd_ins, PERF_EVENT_IOC_ENABLE,0);
	}

	double sum = 0.0012;
	if (!adhoc) {
		for(i = 0; i < workerLoopCount; i++) {
			sum += 0.01;
//			sum *= 0.11;
//			sum -= 0.001;
//			sum /= 2.1;
//			sum += 0.7;
		}	
	}
	else {
		sleep(1);
		start = 1;
	}
 
	if (multithreaded) {
		ioctl(fd_ins, PERF_EVENT_IOC_DISABLE,0);
	}

	if (!adhoc) {
	        printf("Worker thread sum = %f\n", sum);
	}

	if (multithreaded) {
		long long value, value2;
		int read_result=read(fd_ins,&value,sizeof(long long));
		if (read_result != sizeof(long long)) {
			fprintf(stderr, "read not working\n");
			exit(1);
		}
	
		read_result=read(fd_br,&value2,sizeof(long long));
		if (read_result != sizeof(long long)) {
			fprintf(stderr, "read not working\n");
			exit(1);
		}

		close(fd_ins);
		close(fd_br);

		printf("Overflows:\n");
		for(i=0;i<overflows;i++) {
			printf("\t%d %lld %lld\n",i,overflow_counts[i], br_counts[i]);
		}

		printf("f: Counter1 = %lld, Counter2 = %lld\n", value, value2);
	}
	return NULL;
}

int main(int argc, char** argv) {

	int i;
	pthread_t t;

	struct perf_event_attr pe, pe2;

	struct sigaction sa;

	if (argc == 6) {
		sample_period = atoi(argv[1]);
		sample_period_new = atoi(argv[2]);
		workerLoopCount = atoi(argv[3]);
		adhoc = atoi(argv[4]);
		multithreaded = atoi(argv[5]);
	}
	else {
		printf("Run: ./ioctl_period sp1 sp2 loopCount adhoc multithreaded\n");
		exit(1);
	}

	pthread_create(&t, NULL, f, NULL);

	
	memset(&pe,0,sizeof(struct perf_event_attr));

	pe.type=PERF_TYPE_RAW;
	pe.config=(0x00C0ULL) | (0x0000ULL); //instructions retired
	pe.disabled=1;
	pe.exclude_kernel=1;
	pe.exclude_hv=1;
	pe.sample_period=sample_period;
	pe.wakeup_events=1;
	fd_ins=perf_event_open(&pe,0,-1,-1,0);

	/* Set up overflow */
	memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_sigaction = our_handler;
	sa.sa_flags = SA_SIGINFO;
	if (sigaction( SIGRTMIN+2, &sa, NULL) < 0) {
		printf("Error setting up signal handler\n");
	}
	
	memset(&pe2,0,sizeof(struct perf_event_attr));

	pe2.type=PERF_TYPE_RAW;
	pe2.config=(0x00C4ULL) | (0x0100ULL);  // branches retired
	pe2.disabled=0;
	pe2.exclude_kernel=1;
	pe2.exclude_hv=1;
	pe2.sample_period=sample_period;
	pe2.wakeup_events=1;
	fd_br=perf_event_open(&pe2,0,-1,fd_ins,0);

	fdi_m = fd_ins;
	fdb_m = fd_br;

	fcntl(fd_ins, F_SETFL, O_RDWR|O_NONBLOCK|O_ASYNC);
	fcntl(fd_ins, F_SETSIG, SIGRTMIN+2);
	fcntl(fd_ins, F_SETOWN,gettid());

	ioctl(fd_ins, PERF_EVENT_IOC_RESET,0);
	ioctl(fd_br, PERF_EVENT_IOC_RESET,0);
	ioctl(fd_ins, PERF_EVENT_IOC_ENABLE,0);

	double sum = 0.0012;
	if (!adhoc) {
		for(i = 0; i < workerLoopCount; i++) {
			sum += 0.01;
//			sum *= 0.11;
//			sum -= 0.001;
//			sum /= 2.1;
//			sum += 0.7;
		}
	}
	else {
		while(!start) {}
	}
 
	ioctl(fd_ins, PERF_EVENT_IOC_DISABLE,0);

	if (!adhoc) {
	        printf("Worker thread sum = %f\n", sum);
	}

	long long value, value2;
	int read_result=read(fd_ins,&value,sizeof(long long));
	if (read_result != sizeof(long long)) {
		fprintf(stderr, "read not working\n");
		exit(1);
	}
	
	read_result=read(fd_br,&value2,sizeof(long long));
	if (read_result != sizeof(long long)) {
		fprintf(stderr, "read not working\n");
		exit(1);
	}

	pthread_join(t, NULL);

	close(fd_ins);
	close(fd_br);

	printf("Overflows:\n");
	for(i=0;i<overflows;i++) {
		printf("\t%d %lld %lld\n",i,overflow_counts[i], br_counts[i]);
	}

	printf("Counter1 = %lld, Counter2 = %lld\n", value, value2);

	return 0;
}
