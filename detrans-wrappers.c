#include "detrans.h"
#include <asm/unistd.h>
#include <fcntl.h>
#include <linux/perf_event.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#ifndef SERIAL_RUN
static __thread long start_count;
static __thread long commit_count;

#ifdef ADHOC_SUPPORT
static __thread int perf_counter_fds[2];
//static __thread int perf_counter_started;
#endif

__attribute__((transaction_pure))
static long get_start_count() {
  return start_count;
}

__attribute__((transaction_pure))
static void inc_start_count() {
  start_count++;
}

__attribute__((transaction_pure))
static long get_commit_count() {
  return commit_count;
}
 
__attribute__((transaction_pure))
static void inc_commit_count() {
  commit_count++;
}
#endif
 
#ifndef SERIAL_RUN
__attribute__((transaction_pure))
void push_back_from_front() {
  ready_list->push_back(ready_list->front());
}
#endif

extern "C" {
GENERATE_WRAPPER(int, nanosleep, (const struct timespec *req, struct timespec *rem));
GENERATE_WRAPPER(int, pthread_create, (pthread_t *thread,
                                       const pthread_attr_t *attr,
                                       void *(*start_routine)(void*),
                                       void *arg));
GENERATE_WRAPPER(int, pthread_yield, ());
GENERATE_WRAPPER(int, pthread_join, (pthread_t t, void**retval));
GENERATE_WRAPPER(int, pthread_barrier_init,
                 (pthread_barrier_t *barrier,
                  const pthread_barrierattr_t *attr,
                  unsigned count));
GENERATE_WRAPPER(int, pthread_barrier_destroy, (pthread_barrier_t *barrier));
GENERATE_WRAPPER(int, pthread_barrier_wait, (pthread_barrier_t *barrier));
GENERATE_WRAPPER(void, pre_stm_start, ());
GENERATE_WRAPPER(void, post_stm_start, ());
GENERATE_WRAPPER(void, pre_stm_commit, ());
GENERATE_WRAPPER(void, post_stm_commit, ());
} // extern "C"

#ifdef DEBUG
// Nothing
#elif defined DEBUG2

static list<pthread_t> map_to_list(unordered_map<pthread_t, vector<pthread_t> > b) {
  list<pthread_t> out;
  for (auto e : b) {
    if (!e.second.empty()) {
      out.push_back(e.first);
    }
  }
  return out;
}


void print_list_unlocked(list<pthread_t> l, const char*s) {
  DEBUG_PRINT2("%s[", s);
  for (auto i : l) {
    if (0) { fprintf(stderr, "%s%ld\n", s, i); }  // for the compiler warning
    DEBUG_PRINT2(" %ld", i);
  }
  DEBUG_PRINT2("]\n");
}

void print_state() {
  print_list_unlocked(*ready_list, "list");
  print_list_unlocked(zombies, "zombies");
  print_list_unlocked(*blocked_txns, "blocked txns");
  print_list_unlocked(map_to_list(blocked), "blocked");
}

#endif


void erase(list<pthread_t> &vec, pthread_t elem) {
  vec.erase(std::remove(vec.begin(), vec.end(), elem), vec.end());
}

int contains(list<pthread_t> l, pthread_t elem) {
  return count(l.begin(), l.end(), elem) > 0;
}

__attribute__((transaction_pure)) 
void give_turn(pthread_t id) {
  assert(id == ready_list->front());
#ifdef ADHOC_SUPPORT
  perf_counter_disable();
#endif
  ready_list->pop_front();
  DEBUG_PRINT("exit turn %lu\n", tid - first_tid);
 
  if (ready_list->empty()) {
    for (pthread_t &t : *blocked_txns) {
      ready_list->push_back(t);
      DEBUG_PRINT2("unblock txn %lu\n", t);
    }
    blocked_txns->clear();
  }
  UNLOCK(m);
} 

__attribute__((transaction_pure))
void wait_turn(pthread_t id) {
  while (1) {
    LOCK(m);
    assert(!ready_list->empty());
    if (id == ready_list->front()) break;
    UNLOCK(m);
    YIELD(); 
  }
#ifdef ADHOC_SUPPORT
//  perf_counter_enable();
  perf_counter_restart();
#endif
  DEBUG_PRINT("enter turn %lu\n", tid - first_tid);
}

extern "C"
int pthread_yield() {
  ready_list->push_back(ready_list->front());
  give_turn(tid);
  wait_turn(tid);
  return 0;
}

static void* helper(void* arg) {
  void* ret;
  my_pair p = * (my_pair*) arg;
  free(arg);
  tid = pthread_self();

#ifdef ADHOC_SUPPORT
  perf_event_init();
#endif

  wait_turn(tid);

#ifdef ADHOC_SUPPORT
  perf_counter_enable();
#endif

  ret = p.f(p.arg);

  DEBUG_PRINT("thread exiting %ld\n", tid);
  DEBUG_PRINT_STATE();

  if (!blocked[tid].empty()) {
    // blocked thread will join on our `tid'
    assert(blocked[tid].size() == 1);
    blocked[tid].pop_back();
    assert(blocked[tid].empty());
    DEBUG_PRINT2("blocked erasing %ld\n", tid);
    UNLOCK(m);
  } else {
    zombies.push_back(tid);
    give_turn(tid);
  }

#ifdef ADHOC_SUPPORT
  perf_event_close();
#endif

  return ret;
}

extern "C"
int pthread_create(pthread_t *thread,
                   const pthread_attr_t *attr,
                   void *(*start_routine)(void*),
                   void *arg) {
  int ret;
  my_pair* p = (my_pair*)malloc(sizeof(my_pair));
  p->arg = arg;
  p->f = start_routine;
  ret = pthread_create_orig(thread, attr, helper, (void*)p);
  ready_list->push_back(*thread);
  return ret;
}

extern "C"
int pthread_join(pthread_t t, void**retval) {
  int ret;

  DEBUG_PRINT2("about %lu\n", t);
  DEBUG_PRINT_STATE();

  if (contains(zombies, t)) {
    DEBUG_PRINT2("zombies erasing %lu\n", t);
    erase(zombies, t);
    DEBUG_PRINT_STATE();
    ret = pthread_join_orig(t, retval);
  } else {
    DEBUG_PRINT2("blocking %ld\n", tid);
    blocked[t].push_back(tid);
    DEBUG_PRINT_STATE();
    give_turn(tid);
    ret = pthread_join_orig(t, retval);
    LOCK(m);

    assert(contains(*ready_list, t));
    assert(ready_list->front() == t);
    ready_list->pop_front();
    assert(!contains(*ready_list, tid));
    ready_list->push_front(tid);

    DEBUG_PRINT2("waiting %lu\n", t);
  }
  DEBUG_PRINT2("joined %lu\n", t);
  return ret;
}

extern "C"
int pthread_barrier_destroy(pthread_barrier_t *barrier) {
  barriers_size.erase(barrier);
  return pthread_barrier_destroy_orig(barrier);
}

extern "C"
int pthread_barrier_init(pthread_barrier_t * barrier,
                         const pthread_barrierattr_t * attr,
                         unsigned count) {
  barriers_size[barrier] = count;
  return pthread_barrier_init_orig(barrier, attr, count);
}

extern "C"
int pthread_barrier_wait(pthread_barrier_t *barrier) {
  int ret;
  DEBUG_PRINT2("barr  %ld\n", tid);
  barriers[barrier].push_back(tid);
  if (barriers[barrier].size() == barriers_size[barrier]) {
    for (auto b : barriers[barrier]) {
      ready_list->push_back(b);
    }
    barriers[barrier].clear();
  }
  give_turn(tid);
  ret = pthread_barrier_wait_orig(barrier);
  wait_turn(tid);
  return ret;
}

__attribute__((constructor))
static void init_barrier() {
  pthread_barrier_init_init();
}

#ifndef SERIAL_RUN
extern "C"
void pre_stm_start() {
  if (!preloaded) return;
  assert(start_count == 0);
  assert(commit_count == 0);
  DEBUG_PRINT("stm_start() tid %lu\n", (unsigned long) tid);
  DEBUG_PRINT2("begin: tid %lu start_count %ld\n", (unsigned long) tid, get_start_count());
  DEBUG_PRINT_STATE();
#ifndef TINY_RUN
  if (get_start_count() == 0) {
    blocked_txns->push_back(tid);
    give_turn(tid);
    wait_turn(tid);
    push_back_from_front();
    give_turn(tid);
  }
  inc_start_count();
#endif
}

extern "C"
 void post_stm_start() {
  if (!preloaded) return;
#ifdef TINY_RUN
  if (get_start_count() == 0) {
    blocked_txns->push_back(tid);
    give_turn(tid);
    wait_turn(tid);
    push_back_from_front();
    give_turn(tid);
  }
  inc_start_count();
#endif
}

extern "C"
void pre_stm_commit() {
  if (!preloaded) return;
#ifndef TINY_RUN
  if (get_commit_count() == 0) {
   wait_turn(tid);
  }
#endif
  inc_commit_count();
}

extern "C"
void post_stm_commit() {
  if (!preloaded) return;
#ifndef TINY_RUN
  DEBUG_PRINT("stm_commit: tid %lu\n", (unsigned long) tid - (unsigned long) first_tid);
#endif
  start_count = 0;
  commit_count = 0;
#ifdef TINY_RUN
//  assert(ret);
  wait_turn(tid);
  pthread_yield();

  DEBUG_PRINT("tiny-eager: stm_commit: tid %lu\n", (unsigned long) tid - (unsigned long) first_tid);
#else
  pthread_yield();
#endif
}
#else
extern "C"
void pre_stm_start() {
  pthread_yield();
}
#endif

#ifdef ADHOC_SUPPORT
static long perf_event_open(struct perf_event_attr* event_attr, pid_t pid, int cpu, int group_fd, unsigned long flags) {
   return syscall(__NR_perf_event_open, event_attr, pid, cpu, group_fd, flags);
}

static void perf_event_handler(int signum, siginfo_t* info, void* ucontext) {
  if (tid == ready_list->front()) {
    fprintf(stderr, "In the handler %d %lu %lu\n", info->si_fd, (unsigned long) tid, (unsigned long) ready_list->front());
    ready_list->push_back(ready_list->front());
    give_turn(tid);
    wait_turn(tid);
  }
//  perf_counter_restart();
}

void perf_event_init() {
  long sp = SAMPLE_PERIOD;
  // Configure signal handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_sigaction = perf_event_handler;
  sa.sa_flags = SA_SIGINFO;

  // Setup signal handler
  if (sigaction(SIGIO, &sa, NULL) < 0) {
    fprintf(stderr,"Error setting up signal handler\n");
    perror("sigaction");
    exit(EXIT_FAILURE);
  }

  // Configure perf_event_attr struct
  struct perf_event_attr pe;
  memset(&pe, 0, sizeof(struct perf_event_attr));
  pe.type = PERF_TYPE_RAW;
  pe.size = sizeof(struct perf_event_attr);
  pe.config = (0x00C4ULL) | (0x0100ULL); // Count retired branches 
  pe.disabled = 1;        // Event is initially disabled
  pe.sample_period = sp;
  pe.exclude_kernel = 1;      // excluding events that happen in the kernel-space
  pe.exclude_hv = 1;          // excluding events that happen in the hypervisor

  int fd = (int)perf_event_open(&pe, 0, -1, -1, 0);
  if (fd == -1) {
    fprintf(stderr, "Error opening leader %llx\n", pe.config);
    perror("perf_event_open");
    exit(EXIT_FAILURE);
  }

  pe.type = PERF_TYPE_RAW;
  pe.size = sizeof(struct perf_event_attr);
  pe.config = (0x00C0ULL) | (0x0000ULL); // Count retired instructions
  pe.disabled = 1;        // Event is initially disabled
  pe.exclude_kernel = 1;      // excluding events that happen in the kernel-space
  pe.exclude_hv = 1;          // excluding events that happen in the hypervisor

  int fd2 = (int)perf_event_open(&pe, 0, -1, fd, 0);
  if (fd2 == -1) {
    fprintf(stderr, "Error opening leader %llx\n", pe.config);
    perror("perf_event_open");
    exit(EXIT_FAILURE);
  }

  // Setup event handler for overflow signals
  fcntl(fd, F_SETFL, O_NONBLOCK|O_ASYNC);
  fcntl(fd, F_SETSIG, SIGIO);
  fcntl(fd, F_SETOWN, getpid());

  ioctl(fd, PERF_EVENT_IOC_RESET, 0);     // Reset event counter to 0
  ioctl(fd2, PERF_EVENT_IOC_RESET, 0);     // Reset event counter to 0

  ioctl(fd, PERF_EVENT_IOC_REFRESH, 1);   // 
  ioctl(fd2, PERF_EVENT_IOC_REFRESH, 1);   //

  ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);   // Disable event
//  ioctl(fd2, PERF_EVENT_IOC_DISABLE, 0);   // Disable event

  perf_counter_fds[0] = fd;
  perf_counter_fds[1] = fd2;

  fprintf(stderr, "event_init %d %d %d\n", perf_counter_fds[0], perf_counter_fds[1], SAMPLE_PERIOD);
}

void perf_counter_enable() {
  fprintf(stderr, "counter_enable fd = %d, fd2 = %d\n", perf_counter_fds[0], perf_counter_fds[1]);
  int ret = ioctl(perf_counter_fds[0], PERF_EVENT_IOC_ENABLE, 0);
  if (ret < 0) fprintf(stderr, "Error enabling fd %d\n", perf_counter_fds[0]); 

  ret = ioctl(perf_counter_fds[1], PERF_EVENT_IOC_ENABLE, 0);
  if (ret < 0) fprintf(stderr, "Error enabling fd %d\n", perf_counter_fds[1]);
}

void perf_counter_restart() {
  fprintf(stderr, "counter_restart fd = %d, fd2 = %d\n", perf_counter_fds[0], perf_counter_fds[1]);
  ioctl(perf_counter_fds[0], PERF_EVENT_IOC_RESET, 0);
  ioctl(perf_counter_fds[1], PERF_EVENT_IOC_RESET, 0);
  ioctl(perf_counter_fds[0], PERF_EVENT_IOC_REFRESH, 1);
  ioctl(perf_counter_fds[1], PERF_EVENT_IOC_REFRESH, 1);
}

void perf_counter_disable() {
  fprintf(stderr, "counter_disable fd = %d, fd2 = %d\n", perf_counter_fds[0], perf_counter_fds[1]);
  ioctl(perf_counter_fds[0], PERF_EVENT_IOC_DISABLE, 0);   // Disable event
  ioctl(perf_counter_fds[1], PERF_EVENT_IOC_DISABLE, 0);   // Disable event
}

void perf_event_close() {
  long long counter;
  fprintf(stderr, "event_close %d %d\n", perf_counter_fds[0], perf_counter_fds[1]);

  read(perf_counter_fds[0], &counter, sizeof(long long));  // Read event counter value
  fprintf(stderr, "Used %lld branches with fd = %d\n", counter, perf_counter_fds[0]);
  close(perf_counter_fds[0]);

  read(perf_counter_fds[1], &counter, sizeof(long long));  // Read event counter value
  fprintf(stderr, "Used %lld instructions with fd = %d\n", counter, perf_counter_fds[1]);
  close(perf_counter_fds[1]);
}
#endif

__attribute__((constructor))
static void init() {
  LOCK_INIT(m);
  LOCK(m);
  tid = pthread_self();
  nanosleep_init();
  pthread_create_init();
  pthread_join_init();
  pthread_yield_init();
  pthread_barrier_destroy_init();
  pthread_barrier_init_init();
  pthread_barrier_wait_init();
  pre_stm_start_init();
  post_stm_start_init();
  pre_stm_commit_init();
  post_stm_commit_init();
  ready_list = unique_ptr<list<pthread_t> > (new list<pthread_t>);
  ready_list->push_front(tid);
  blocked_txns = unique_ptr<list<pthread_t> > (new list<pthread_t>);
  preloaded = true;

#ifdef ADHOC_SUPPORT
  perf_event_init();
//  perf_counter_enable();
#endif

}

__attribute__((destructor))
static void post_exit() {

#ifdef ADHOC_SUPPORT
  perf_counter_disable();
  perf_event_close();
#endif

  UNLOCK(m);
  LOCK_DESTROY(m);
  DEBUG_PRINT_STATE();
}
